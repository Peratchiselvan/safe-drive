# Safe Drive
 **An app to make everyone safe during driving.**
 
## How it works?
This app hungs the call of the user when he attends the call while driving.
## Features:
- [x] Hung the call when the call is attended.
- [ ] Use gps services to hung the call when the user is driving.
### Note:
> Feel free to contribute. For any doubts [contact me in telegram.](https://t.me/selvasoft_ceo)

> Note that this app uses GNU GPL V3 license which means if you use this app or made any derivative app, you must give proper credits and open source your app with same license to comply the license. 